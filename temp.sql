CREATE TABLE api(
     id MEDIUMINT NOT NULL AUTO_INCREMENT,
     rulename CHAR(30) NOT NULL,
     
     PRIMARY KEY (id)
);

INSERT INTO api (rulename) VALUES
     ('Must be 5 characters'), ('Must not be used'), ('Must be cool');
     
UPDATE api SET rulename='Must be cool test'
WHERE id=3;     