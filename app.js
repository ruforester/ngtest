var express = require("express");
var mysql = require("mysql");
var bodyParser = require("body-parser");


var app = express();

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({
    extended: true
})); // support encoded bodies

app.use(express.static(__dirname + '/static'))

app.get('/', function(req, res) {
    res.sendFile(__dirname + '/index.html');
});


function mysqlQuery(method, req, res) {

    var connection = mysql.createConnection({
        host: 'localhost',
        user: 'ruforester',
        password: '',
        database: 'c9'
    });

    connection.connect();

    var queryStringGET = 'SELECT * FROM api';
    var queryStringPOST = '';


    if (method === 'POST') {
        
        queryStringPOST = 'INSERT INTO api (rulename) VALUES ("' + req.body.newRule + '")';
        connection.query(queryStringPOST, function(err) {
            if (err) throw err;
            
                connection.query(queryStringGET, function(err, rows, fields) {
                    if (err) throw err;
                    res.json(rows);
                    connection.end();
                });
        })
    }
    else if (method === 'GET') {
        connection.query(queryStringGET, function(err, rows, fields) {
            if (err) throw err;
            res.json(rows);
            connection.end();
        });
    } else {
        connection.end();
    }

    
}

app.get('/api', function(req, res) {
    mysqlQuery('GET', req, res);
});

app.post('/api', function(req, res) {
    mysqlQuery('POST', req, res);
});

app.get('/favicon.ico', function(req, res) {
    res.sendFile(__dirname + '/favicon.ico');
});

//for local use; comment on cloud9
process.env.PORT = 3000;

app.listen(process.env.PORT, function() {
    console.log('Listening on port ' + process.env.PORT);
});
